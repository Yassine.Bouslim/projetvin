#! /usr/bin/env python3
from flask import Flask
app=Flask(__name__)
app.debug=True
app.config['BOOTSTRAP_SERVE_LOCAL']=True

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)
from flask.ext.script import Manager
manager=Manager(app)

import os.path
def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))
	
from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']= (
	'sqlite:///'+mkpath('../vins.db'))
db=SQLAlchemy(app)

app.config['SECRET_KEY'] = "4d8c6df0-f9b1-436d-9b68-8b056c555b5a"

from flask.ext.login import LoginManager
login_manager = LoginManager(app)

login_manager.login_view = "login"

from flask.ext.admin import Admin, BaseView, expose
admin = Admin(app)
