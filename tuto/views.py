#-*- coding: utf-8 -*-
#! /usr/bin/env python3
from .app import app, db
from sqlalchemy import func
from flask import url_for, redirect, render_template
from .models import get_sample, Vin, Commande, User
import yaml,os

@app.route("/")
def home():
	data = get_sample() #yaml.load(open(os.path.join(os.path.dirname(__file__),'data.yml'))) #Il faut remettre le get_sample c'est juste pour tester si l'affichage marche
	les_categories = ["Toutes"]
	f=QteForm()
	for d in data :
		if d.categorie not in les_categories :
			les_categories.append(d.categorie)
	return render_template(
	"home.html",
	title="Soulmanie",
	form=f,
	donnees=data,
	categories=les_categories,
	)
	
	
@app.route("/categorie/<categorie>")
def trivins(categorie):
	tabvins=[]
	data=get_sample()
	les_categories = ["Toutes"]
	for d in data :
		if d.categorie not in les_categories :
			les_categories.append(d.categorie)
	if categorie=="Toutes":
		return render_template(
		"home.html",
		title="Soulmanie",
		donnees=data,
		categories=les_categories,
		lacategorie=categorie,
	)	
	for vin in data:
		if vin.categorie==categorie:
			tabvins.append(vin)
	return render_template(
	"home.html",
	title="Soulmanie",
	donnees=tabvins,
	categories=les_categories,
	lacategorie=categorie,
	form = QteForm()
	)
	
@app.route("/panier/<pseudo>")
def panier(pseudo):
	data=Commande.query.filter_by(client_reference=pseudo)
	return render_template(
	"panier.html",
	title="Votre panier",
	donnees=data,
	)
				

from flask.ext.wtf import Form
from flask.ext.login import login_required
from wtforms import StringField, HiddenField, PasswordField, IntegerField
from wtforms.validators import DataRequired

class UserForm(Form) :
	mail = StringField('ADRESSE MAIL', validators=[DataRequired()])
	pseudo = StringField('PSEUDO', validators=[DataRequired()])
	name = StringField('NOM', validators=[DataRequired()])
	prenom = StringField('PRENOM', validators=[DataRequired()])
	mdp = PasswordField('MOT DE PASSE', validators=[DataRequired()])
	
	def get_user(pseudo):
		return User.get(pseudo)
				

class VinForm(Form) :
	nom = StringField('NOM', validators=[DataRequired()])
	categorie = StringField('CATEGORIE', validators=[DataRequired()])
	pays = StringField('PAYS', validators=[DataRequired()])
	variete = StringField('VARIETE')
	vintage = StringField('VINTAGE')
	region = StringField('REGION', validators=[DataRequired()])
	quantite = IntegerField('QUANTITE', validators=[DataRequired()])
	
	def get_vin(nom):
		return Vin.get(nom)

	
@app.route("/ajout/")
def ajout_author():
	f=UserForm(name=None, prenom=None, mail=None, mdp=None)
	return render_template("ajout-client.html", form=f)


class QteForm(Form):
	quantite=IntegerField('Quantité', validators=[DataRequired()])
	
@app.route("/ajouter/vin/<pseudo>-<nomVin>",methods=("POST",))
def ajouter_vin(nomVin,pseudo):
	f=QteForm()
	c=Commande()
	levin = Vin.query.get(nomVin)
	c.client_reference=pseudo
	c.vin_reference=nomVin
	if int(levin.quantite)-int(f.quantite.data)<=0:
		c.quantite=0
	else :
		c.quantite=f.quantite.data
	if levin : 
		levin.quantite=str(int(levin.quantite)-int(c.quantite))
	db.session.add(c)
	db.session.commit()
	return render_template(
		"panier.html",
		title="Votre panier",
		donnees=Commande.query.filter_by(client_reference=pseudo),
		)
			
from hashlib import sha256
from flask.ext.login import login_user, current_user
@app.route("/ajouter/client/", methods=("POST",))
def ajouter_client():
	a = None
	f = UserForm()
	if User.query.get(f.pseudo.data)!=None:
		msg = "Votre pseudo est deja pris, merci de le changer. Faites travailler votre imagination !"
		return render_template(
			"ajout-client.html",
			form=f,
			message=msg)
	if f.validate_on_submit():
		u = User() #Creation du nouvelle instance
		#Ses parametres
		u.pseudo = f.pseudo.data
		u.usermail = f.mail.data
		u.username = f.name.data
		u.usersurname = f.prenom.data
		m = sha256()
		m.update((f.mdp.data).encode())
		u.password = m.hexdigest()
		#u.usercode = User.query.count()+1
		login_user(u) #je le connecte
		db.session.add(u)
		db.session.commit()
		return render_template('bienvenue.html',nom=u.username)
	msg = "Un probleme dans vos saisies. Un petit effort, recommencez !"
	return render_template(
		"ajout-client.html",
		form=f,
		message=msg)

@app.route("/ajout-vin-base/", methods=("POST",))
def ajouter_vin_administrateur():
	f = VinForm()
	if f.validate_on_submit():
		v = Vin() #Creation du nouvelle instance
		v.nom = f.nom.data
		v.categorie = f.categorie.data
		v.pays = f.pays.data
		v.vintage = f.vintage.data
		v.variete = f.variete.data
		v.region = f.region.data
		v.quantite = str(f.quantite.data)
		db.session.add(v)
		db.session.commit()
		msg = "Creation du vin"+v.nom+" : Succes ! "
		return render_template("ajout-vin.html", form=f, message=msg)
	return render_template(
		"ajout-vin.html",
		form=f,
		)
		
@app.route("/suppression-vin-base/<nomVin>", methods=("POST",))
def supprimer_vin_administrateur(nomVin):
	v = Vin.query.get(nomVin)
	if v is not None : 
		db.session.delete(v)
		db.session.commit()
		data = get_sample() 
		les_categories = ["Toutes"]
		for d in data :
			if d.categorie not in les_categories :
				les_categories.append(d.categorie)
		f=QteForm()
		return render_template(
			"home.html",
			title="Soulmanie",
			form=f,
			donnees=data,
			categories=les_categories,
			)
	return redirect("home.html",)



from wtforms.ext.appengine.db import model_form
@app.route("/modification-vin-base/<nomVin>", methods=("POST",))
def modifier_vin_administrateur(nomVin):
	v=Vin.query.get(nomVin)
	f=VinForm(obj=v)
	if f.validate_on_submit():
		v.nom = f.nom.data
		v.categorie = f.categorie.data
		v.pays = f.pays.data
		v.vintage = f.vintage.data
		v.variete = f.variete.data
		v.region = f.region.data
		v.quantite = str(f.quantite.data)
		db.session.commit()
		msg = "Modification du vin"+v.nom+" : Succes ! "
		return render_template("modifier-vin.html",form=f,message=msg, vin=v.nom)
	return render_template("modifier-vin.html",form=f,vin=v.nom)

@app.route("/suppression-vin/<pseudo>-<nomVin>", methods=("POST",))
def supprimer_vin_commande(pseudo,nomVin):
	c = Commande.query.filter_by(client_reference=pseudo,vin_reference=nomVin)
	for i in c : 
		db.session.delete(i)
		db.session.commit()
	return render_template("panier.html",
		title="Votre panier",
		donnees=Commande.query.filter_by(client_reference=pseudo),
		)

@app.route("/suppression-commandes/<pseudo>", methods=("POST",))
def supprimer_commandes(pseudo):
	c = Commande.query.filter_by(client_reference=pseudo)
	for i in c : 
		db.session.delete(i)
		db.session.commit()
	return render_template("panier.html",
		title="Votre panier",
		donnees=Commande.query.filter_by(client_reference=pseudo),
		)
		
@app.route("/bienvenue")		
def bienvenue(name):
	return render_template("bienvenue.html",nom=name)

@app.route("/contact")
def contact():
	return render_template("contact.html")
	
#Authentification


from .models import User

class LoginForm(Form):
	pseudo = StringField("Nom d'utilisateur (pseudo)")
	password = PasswordField('Mot de passe')
	next = HiddenField()
	
	def get_authenticated_user(self):
		user = User.query.get(self.pseudo.data)
		if user is None :
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None
		

from flask import request

@app.route("/login/", methods=("GET","POST",))
def login():
	f = LoginForm()
	if not f.is_submitted() : 
		f.next.data = request.args.get("next")
	elif f.validate_on_submit():
		inscrit = f.get_authenticated_user()
		if inscrit is not None : 
			login_user(inscrit)
			next = f.next.data or url_for("home")
			return redirect(next)
		else : 
			return render_template("login.html", form = f, message="Pseudo/Mot de passe incorrecte(s). Veuillez recommencer...")
	return render_template(
		"login.html",
		form = f,)
		

from flask.ext.login import logout_user

@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))
