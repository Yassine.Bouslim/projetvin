from .app import manager,db

@manager.command
def loaddb(filename):
	'''Create the tables and populates them with data'''
	
	db.create_all()
	
	import yaml
	vins = yaml.load(open(filename))
	yaml.dump(vins, encoding=('utf-8'))
	from .models import User,Vin
	
	#~ authors={}
	#~ 
	#~ for b in books:
		#~ a=b["author"]
		#~ if a not in authors:
			#~ o=Author(name=a)
			#~ db.session.add(o)
			#~ authors[a]=o
		#~ db.session.commit()
		#~ 
	#~ for b in books:
		#~ a=authors[b["author"]]
		#~ o=Book(price=b["price"],
				#~ title=b["title"],
				#~ url=b["url"],
				#~ img=b["img"],
				#~ author_id=a.id)
		#~ db.session.add(o)
	#~ db.session.commit()
	
	listevin={}
	for v in vins:
		a=v["name"]
		if a not in listevin:
			o=Vin(categorie=v["category"],
			pays=v["country"],
			image=v["image"],
			nom=v["name"],
			quantite=v["quantity"],
			region=v["region"],
			variete=v["varietal"],
			vintage=v["vintage"],	
			)
			listevin[a]=o
			db.session.add(o) 
	db.session.commit()
		

@manager.command
def syncdb():
	'''Creates all missing tables '''
	db.create_all()

@manager.command
def newuser(usermail, username, usersurname, password):
	''' Adds a new user '''
	from .models import User
	from hashlib import sha256
	m = sha256()
	m.update(password.encode())
	u = User(username=username, usersurname=usersurname, usermail=usermail, password=m.hexdigest())
	db.session.add(u)
	db.session.commit()
